﻿using Library.Characteristics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Borrowing
{
    public class Borrower
    {
        private int months_per_year = 12;
        private double monthly_payments_with_insurance;
        private double monthly_payments_without_insurance;

        public Capital Capital { get; }
        public RefundDuration Duration { get; }
        public Interest Interest { get; }
        public Insurance Insurance { get; }

        public Borrower(Capital capital, RefundDuration duration, Interest interest, Insurance insurance) {

            Capital = capital;
            Duration = duration;
            Interest = interest;
            Insurance = insurance;
            monthly_payments_with_insurance = CalculateMonthlyPaymentsWithInsurance();
            monthly_payments_without_insurance = CalculateMonthlyPaymentsWithoutInsurance();
        }
        public int GetMonthlyInsurancePayment()
        {
            return (int)Math.Round(monthly_payments_with_insurance);
        }

        public double CalculateMonthlyPaymentsWithInsurance()
        {
            return Math.Round(Capital * (Insurance / 100f) / months_per_year);
        }

        public double CalculateMonthlyPaymentsWithoutInsurance()
        {
            double total_interest = Interest / 100f;
            double capital_add = Math.Round(Capital * (total_interest / months_per_year));
            double monthly_payment = Math.Pow(1 + (total_interest / months_per_year), -months_per_year * Duration);
            return monthly_payment / (1 - capital_add);
        }

        public int CalculateTotalInsurance()
        {
            return (int)Math.Round(monthly_payments_with_insurance * Duration * months_per_year);
        }

        public int CalculateMonthlyPayment()
        {
            return (int)Math.Round(monthly_payments_without_insurance + monthly_payments_with_insurance);
        }

        //  A vérifier si besoin de l'assurance ou non dans le calcul
        public int CalculateTotalInterest()
        {
            return (int)Math.Round((months_per_year * Duration * (monthly_payments_without_insurance + monthly_payments_with_insurance)) - Capital);
        }

        public int CalculateCapitalOnRefundDuration(uint years)
        {
            int capital_amount = (int)(CalculateMonthlyPayment() * months_per_year * years);

            return capital_amount;
        }
    }
}
