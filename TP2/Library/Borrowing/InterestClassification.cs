﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Borrowing
{
    public enum InterestClassification
    {
        GOOD_RATIO,
        VERY_GOOD_RATIO,
        EXCELLENT_RATIO
    }
}
