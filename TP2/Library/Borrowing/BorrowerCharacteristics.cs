﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Borrowing
{
    public struct BorrowerCharacteristics
    {
        public BorrowerCharacteristics(bool sporty, bool smoker, bool heartProblems, bool iTEngineer, bool fighterPilot)
        {
            Sporty = sporty;
            Smoker = smoker;
            HeartProblems = heartProblems;
            ITEngineer = iTEngineer;
            FighterPilot = fighterPilot;
        }

        public bool Sporty { get; set; } = false;
        public bool Smoker { get; set; } = false;
        public bool HeartProblems { get; set; } = false;
        public bool ITEngineer { get; set; } = false;
        public bool FighterPilot { get; set; } = false;
    }
}
