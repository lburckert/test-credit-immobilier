﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Characteristics
{
    public struct Interest
    {
        private static float min_interest = 0f;
        private static float max_interest = 2f;

        public Interest(float value)
        {
            this.Value = value;
        }

        public static implicit operator Interest(float value) => new(value);
        public static implicit operator float(Interest value) => value.Value;

        public float Value { get; }
    }
}
