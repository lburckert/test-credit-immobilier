﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Characteristics
{
    public struct RefundDuration
    {
        private static uint min_duration = 9;
        private static uint max_duration = 25;

        public RefundDuration(float value)
        {
            this.Value = value;
        }

        public static implicit operator RefundDuration(float value) => new(value);
        public static implicit operator float(RefundDuration value) => value.Value;

        public float Value { get; }
    }
}
