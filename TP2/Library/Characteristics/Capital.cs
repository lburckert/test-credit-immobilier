﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Characteristics
{
    public struct Capital
    {
        private static uint min_capital = 50000;
        private static uint max_capital = 500000;

        public Capital(float value)
        {
            this.Value = value;
        }

        public static implicit operator Capital(float value) => new(value);
        public static implicit operator float(Capital value) => value.Value;

        public float Value { get; }
    }
}
