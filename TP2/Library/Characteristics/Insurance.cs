﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Characteristics
{
    public struct Insurance
    {
        private static float min_insurance = 0.1f;
        private static float max_insurance = 0.9f;

        public Insurance(float value)
        {
            this.Value = value;
        }

        public static implicit operator Insurance(float value) => new(value);
        public static implicit operator float(Insurance value) => value.Value;

        public float Value { get; }
    }
}
