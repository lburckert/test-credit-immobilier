﻿using Library.Characteristics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.CalculatorsBase
{
    public class InterestGoodBase : InterestBasesCalculator
    {
        protected override Interest CalculateInterestRatio(RefundDuration duration)
        {
            int[] durations = new[] { 7, 10, 15, 20, 25 };
            float[] rates = new[] { 0.62f, 0.67f, 0.85f, 1.04f, 1.27f };

            int index_base = GetCorrespondingIndex(duration, durations, rates);

            return rates[index_base];
        }
    }
}
