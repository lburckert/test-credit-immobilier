﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Characteristics;
using Library.Borrowing;

namespace Library.CalculatorsBase
{
    public abstract class InterestBasesCalculator
    {
        protected abstract Interest CalculateInterestRatio(RefundDuration duration);

        public static Interest CalculateInterestRatio(RefundDuration duration, InterestClassification interest_classification)
        {
            InterestBasesCalculator InterestCalculator = interest_classification switch
            {
                InterestClassification.GOOD_RATIO => new InterestGoodBase(),
                InterestClassification.VERY_GOOD_RATIO => new InterestVeryGoodBase(),
                InterestClassification.EXCELLENT_RATIO => new InterestExcellentBase(),
                _ => throw new NotImplementedException()
            };
            return InterestCalculator.CalculateInterestRatio(duration);
        }

        protected static int GetCorrespondingIndex(RefundDuration duration, int[] durations, float[] rates)
        {
            int min = int.MaxValue;
            int index_base = rates.Length - 1;
            
            for (int i = 0; i < durations.Length; i++)
            {
                if (Math.Abs(duration - durations[i]) < min)
                {
                    min = (int)Math.Abs(duration - durations[i]);
                    index_base = i;
                }
            }
            return index_base;
        }
    }
}
