﻿using Library.Characteristics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.CalculatorsBase
{
    internal class InterestExcellentBase : InterestBasesCalculator
    {
        protected override Interest CalculateInterestRatio(RefundDuration duration)
        {
            int[] durations = new[] { 7, 10, 15, 20, 25 };
            float[] rates = new[] { 0.35f, 0.45f, 0.58f, 0.73f, 0.89f };

            int index_base = GetCorrespondingIndex(duration, durations, rates);

            return rates[index_base];
        }
    }
}
