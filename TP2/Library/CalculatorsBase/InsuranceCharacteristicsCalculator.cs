﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Borrowing;
using Library.Characteristics;

namespace Library.CalculatorsBase
{
    public static class InsuranceCharacteristicsCalculator
    {
        private static float sporty = -0.05f;
        private static float smoker = 0.15f;
        private static float heart_problems = 0.3f;
        private static float it_engineer = -0.05f;
        private static float fighter_pilot = 0.15f;
        private static float interest_base = 0.3f;
        
        public static Insurance CalculateInsuranceRatio(BorrowerCharacteristics BorrowerCharacteristics)
        {
            float base_calculation = interest_base;

            if (BorrowerCharacteristics.Sporty)
            {
                base_calculation += sporty;
            }
            if (BorrowerCharacteristics.Smoker)
            {
                base_calculation += smoker;
            }
            if (BorrowerCharacteristics.HeartProblems)
            {
                base_calculation += heart_problems;
            }
            if (BorrowerCharacteristics.ITEngineer)
            {
                base_calculation += it_engineer;
            }
            if (BorrowerCharacteristics.FighterPilot)
            {
                base_calculation += fighter_pilot;
            }

            return base_calculation;
        }
    }
}
