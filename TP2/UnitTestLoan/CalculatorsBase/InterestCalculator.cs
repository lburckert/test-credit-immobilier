﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Library.Borrowing;
using Library.Characteristics;

namespace UnitTestLoan.CalculatorsBase
{
    public class InterestCalculator
    {
        public static IEnumerable<object[]> InterestRatiosData
        {
            get
            {
                yield return new object[] { new RefundDuration(25), InterestClassification.GOOD_RATIO, 1.27f };
                yield return new object[] { new RefundDuration(15), InterestClassification.VERY_GOOD_RATIO, 0.73f };
                yield return new object[] { new RefundDuration(10), InterestClassification.EXCELLENT_RATIO, 0.45f };
            }
        }
    }
}
