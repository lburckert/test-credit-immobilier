﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Library.Borrowing;
using Library.Characteristics;

namespace UnitTestLoan.CalculatorsBase
{
    internal class InsuranceCalculator
    {
        public static IEnumerable<object[]> BorrowerCharacteristics
        {
            get
            {
                yield return new object[] {
                    new BorrowerCharacteristics {
                        ITEngineer = true,
                        Smoker = true,
                        HeartProblems = true
                    }, (- 0.05f + 0.15f + 0.3f)
                };
                yield return new object[] {
                    new BorrowerCharacteristics() {
                        FighterPilot = true,
                        Sporty = true
                    }, (0.15f - 0.05f)
                };
                yield return new object[] {
                    new BorrowerCharacteristics() {
                        Sporty = true,
                        ITEngineer = true
                    }, (- 0.05f - 0.05f)
                };
                yield return new object[] {
                    new BorrowerCharacteristics() {
                        Sporty = true,
                        Smoker = true,
                        ITEngineer = true
                    }, (- 0.05f + 0.15f - 0.05f)
                };
                yield return new object[] {
                    new BorrowerCharacteristics() {
                        ITEngineer = true
                    }, (- 0.05f)
                };
            }
        }
    }
}
