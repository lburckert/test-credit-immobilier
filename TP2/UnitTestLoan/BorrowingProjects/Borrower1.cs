﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Characteristics;
using Library.Borrowing;
using Xunit;

namespace UnitTestLoan.BorrowingProjects
{
    public class Borrower1
    {
        // Cas pratique #1 du TP

        private static Capital offer_capital = 175000;
        private static Interest offer_iterest = 1.27f;
        private static Insurance offer_insurance = 0.4f;
        private static RefundDuration offer_duration = 25;

        private static Borrower borrower = new (offer_capital, offer_duration,  offer_iterest, offer_insurance);

        // Test des données présentes dans la page "BorrowingRecap" de l'interface utilisateur

        [Fact]
        public void IsCalculateCapitalCorrect()
        {
            int capital_amount = borrower.CalculateCapitalOnRefundDuration(10);
            Assert.Equal(93960, capital_amount);
        }

        [Fact]
        public void IsMontlyPaymentCorrect()
        {
            int montly_payment = borrower.CalculateMonthlyPayment();
            Assert.Equal(783, montly_payment);
        }

        [Fact]
        public void IsCalculateTotalInterestsCorrect()
        {
            int total_interest = borrower.CalculateTotalInterest();
            Assert.Equal(59966, total_interest);
        }

        [Fact]
        public void IsCalculateMontlyInsuranceCorrect()
        {
            int montly_insurance = borrower.GetMonthlyInsurancePayment();
            Assert.Equal(102, montly_insurance);
        }

        [Fact]
        public void IsCalculateTotalInsuranceCorrect()
        {
            int total_insurance = borrower.CalculateTotalInsurance();
            Assert.Equal(30625, total_insurance);
        }
    }
}
