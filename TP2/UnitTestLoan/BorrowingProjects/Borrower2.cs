﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Characteristics;
using Library.Borrowing;
using Xunit;

namespace UnitTestLoan.BorrowingProjects
{
    public class Borrower2
    {
        // Cas pratique #2 du TP

        private static Capital offer_capital = 200000;
        private static Interest offer_iterest = 0.73f;
        private static Insurance offer_insurance = 0.4f;
        private static RefundDuration offer_duration = 15;

        private static Borrower borrower = new(offer_capital, offer_duration, offer_iterest, offer_insurance);

        // Test des données présentes dans la page "BorrowingRecap" de l'interface utilisateur

        [Fact]
        public void IsCalculateCapitalCorrect()
        {
            int capital_amount = borrower.CalculateCapitalOnRefundDuration(10);
            Assert.Equal(148800, capital_amount);
        }

        [Fact]
        public void IsMontlyPaymentCorrect()
        {
            int montly_payment = borrower.CalculateMonthlyPayment();
            Assert.Equal(1240, montly_payment);
        }

        [Fact]
        public void IsCalculateTotalInterestsCorrect()
        {
            int total_interest = borrower.CalculateTotalInterest();
            Assert.Equal(23211, total_interest);
        }

        [Fact]
        public void IsCalculateMontlyInsuranceCorrect()
        {
            int montly_insurance = borrower.GetMonthlyInsurancePayment();
            Assert.Equal(67, montly_insurance);
        }

        [Fact]
        public void IsCalculateTotalInsuranceCorrect()
        {
            int total_insurance = borrower.CalculateTotalInsurance();
            Assert.Equal(12000, total_insurance);
        }
    }
}
