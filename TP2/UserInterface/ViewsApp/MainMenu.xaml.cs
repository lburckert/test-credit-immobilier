﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UserInterface.ViewsApp
{
    /// <summary>
    /// Logique d'interaction pour MainMenu.xaml
    /// </summary>
    public partial class MainMenu : Page
    {
        private Frame navigate_frame;
        public MainMenu(Frame main_window_frame_navigate)
        {
            InitializeComponent();
            navigate_frame = main_window_frame_navigate;
        }
        private void Button_Click_Borrower(object sender, RoutedEventArgs e)
        {
            navigate_frame.Content = new BorrowerProfile(navigate_frame);
        }

        private void Button_Click_Exit(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}
