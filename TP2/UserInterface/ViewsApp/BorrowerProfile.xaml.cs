﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Library.Borrowing;

namespace UserInterface.ViewsApp
{
    /// <summary>
    /// Logique d'interaction pour Borrower.xaml
    /// </summary>
    public partial class BorrowerProfile : Page
    {
        private Frame navigate_frame;
        public BorrowerProfile(Frame main_window_frame_navigate)
        {
            InitializeComponent();
            navigate_frame = main_window_frame_navigate;
        }
        private void Button_Click_Back_Main(object sender, RoutedEventArgs e)
        {
            navigate_frame.Content = new MainMenu(navigate_frame);
        }
        private void Button_Click_Form(object sender, RoutedEventArgs e)
        {
            navigate_frame.Content = new Form(navigate_frame);
        }
        public BorrowerCharacteristics BorrowerCharacteristics
        {
            get
            {
                return new BorrowerCharacteristics()
                {
                    Sporty = checkbox_sporty.IsChecked ?? false,
                    Smoker = checkbox_smoker.IsChecked ?? false,
                    HeartProblems = checkbox_heartprb.IsChecked ?? false,
                    ITEngineer = checkbox_engineer.IsChecked ?? false,
                    FighterPilot = checkbox_pilot.IsChecked ?? false              
                };
            }
        }
        /*
                private void Sporty_checked(object sender, RoutedEventArgs e)
                {
                    CheckBox checkbox = (CheckBox)sender;

                    bool isChecked = checkbox.IsChecked ?? false;
                    if (isChecked)
                    {
                        checkbox_sporty.IsChecked = false;
                    }
                }
                private void Smoker_checked(object sender, RoutedEventArgs e)
                {
                    CheckBox checkbox = (CheckBox)sender;

                    bool isChecked = checkbox.IsChecked ?? false;
                    if (isChecked)
                    {
                        checkbox_smoker.IsChecked = false;
                    }
                }
                private void HeartPrb_checked(object sender, RoutedEventArgs e)
                {
                    CheckBox checkbox = (CheckBox)sender;

                    bool isChecked = checkbox.IsChecked ?? false;
                    if (isChecked)
                    {
                        checkbox_heartprb.IsChecked = false;
                    }
                }
                private void Engineer_checked(object sender, RoutedEventArgs e)
                {
                    CheckBox checkbox = (CheckBox)sender;

                    bool isChecked = checkbox.IsChecked ?? false;
                    if (isChecked)
                    {
                        checkbox_engineer.IsChecked = false;
                    }
                }
                private void Pilot_checked(object sender, RoutedEventArgs e)
                {
                    CheckBox checkbox = (CheckBox)sender;

                    bool isChecked = checkbox.IsChecked ?? false;
                    if (isChecked)
                    {
                        checkbox_pilot.IsChecked = false;
                }
        }*/
    }
}
