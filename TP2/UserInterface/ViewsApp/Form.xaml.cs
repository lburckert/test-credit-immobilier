﻿using Library.Characteristics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UserInterface.ViewsApp
{
    /// <summary>
    /// Logique d'interaction pour Form.xaml
    /// </summary>
    public partial class Form : Page
    {
        private Frame navigate_frame;
        public Form(Frame main_window_frame_navigate)
        {
            InitializeComponent();
            navigate_frame = main_window_frame_navigate;
        }
        private void Button_Click_Back_Main(object sender, RoutedEventArgs e)
        {
            navigate_frame.Content = new MainMenu(navigate_frame);
        }
        private void Button_Click_Recap(object sender, RoutedEventArgs e)
        {
            navigate_frame.Content = new BorrowingRecap(navigate_frame, this);
        }
        public Capital Capital => uint.Parse(txtbox_capital.Text);
        public Insurance Insurance => float.Parse(txtbox_interest.Text);
        public Interest Interest => float.Parse(txtbox_interest.Text);
        public RefundDuration Duration => uint.Parse(txtbox_duration.Text);
 

    }
}
