﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Library.Borrowing;

namespace UserInterface.ViewsApp
{
    /// <summary>
    /// Logique d'interaction pour BorrowingRecap.xaml
    /// </summary>
    public partial class BorrowingRecap : Page
    {
        private Frame navigate_frame;
        private Borrower borrower;

        public BorrowingRecap(Frame main_window_frame_navigate, Form form)
        {
            InitializeComponent();
            navigate_frame = main_window_frame_navigate;

            borrower = new Borrower(form.Capital, form.Duration, form.Interest, form.Insurance);
        }
        private void Button_Click_Back_Main(object sender, RoutedEventArgs e)
        {
            navigate_frame.Content = new MainMenu(navigate_frame);
        }

        public void Button_Generate_Click(object sender, RoutedEventArgs e)
        {
            txtbox_monthly_payment.Text = borrower.CalculateMonthlyPayment().ToString();
            txtbox_total_payment.Text = borrower.CalculateTotalInterest().ToString();
            txtbox_monthly_payment_insurance.Text = borrower.CalculateMonthlyPaymentsWithInsurance().ToString();
            txtbox_total_payment_insurance.Text = borrower.CalculateTotalInsurance().ToString();
        }
    }
}
