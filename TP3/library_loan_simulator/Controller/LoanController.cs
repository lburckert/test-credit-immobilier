﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using library_loan_simulator.Model.Loan;

namespace library_loan_simulator.Controller
{
    public class LoanController
    {
        public LoanAmount loanAmount;
        public LoanDuration loanDuration;
        public LoanInsurance loanInsurance;
        public LoanInterest loanInterest;
        public AmortizationTable amortizationTable;
        public LoanDefferedDuration loanDefferedDuration;
        private uint months_per_year = 12;

        public LoanController()
        {
            loanAmount = new LoanAmount();
            loanDuration = new LoanDuration();
            loanInsurance = new LoanInsurance();
            loanInterest = new LoanInterest();
            amortizationTable = new AmortizationTable();
            loanDefferedDuration = new LoanDefferedDuration();
        }

        public double GetMonthlyPaymentTotal()
        {
            return GetMonthlyPaymentWithoutInsurance() + GetMonthlyInsurancePayment();
        }

        public double GetMonthlyPaymentWithoutInsurance()
        {
            double interest = loanInterest.InterestRate / 100f;
            uint loanDurationInMonthWithoutDeffered = (months_per_year * loanDuration.Duration) - loanDefferedDuration.DefferedDuration;
            double capital_add = loanAmount.Amount * (interest / months_per_year);
            double monthly_payment = 1 - Math.Pow(1 + (interest / months_per_year), -loanDurationInMonthWithoutDeffered);
            return capital_add / monthly_payment;
        }

        public double GetMonthlyInsurancePayment()
        {
            double insurance = loanInsurance.InsuranceRate / 100f;
            return loanAmount.Amount * insurance / months_per_year;
        }
        public double GetTotalInterestPayment()
        {
            uint loanDurationInMonthWithoutDeffered = (months_per_year * loanDuration.Duration) - loanDefferedDuration.DefferedDuration;
            double interestAmount = loanDurationInMonthWithoutDeffered * GetMonthlyPaymentWithoutInsurance();
            double firstInterest = (loanAmount.Amount * loanInterest.InterestRate / 100 / months_per_year);
            double differedInterestAmount = firstInterest * loanDefferedDuration.DefferedDuration;
            return  interestAmount + differedInterestAmount - loanAmount.Amount;
        }

        public double GetTotalInsurancePayment()
        {
            return GetMonthlyInsurancePayment() * loanDuration.Duration * months_per_year;
        }

        public double GetRefundAmountAtYear(uint years)
        {
            uint startRefundMonth = loanDefferedDuration.DefferedDuration + 1;
            uint AmountAtMonth = years * months_per_year;
            double interest = 0.0;
            uint loanRefund = 0;
            double amountToPay = loanAmount.Amount;
            double monthlyPayment = GetMonthlyPaymentWithoutInsurance();
            for (uint monthNumber = startRefundMonth; monthNumber <= AmountAtMonth; monthNumber++)
            {
                interest = (amountToPay * loanInterest.InterestRate / 100 / months_per_year);
                loanRefund += (uint)(Math.Round(monthlyPayment) - Math.Round(interest));
                amountToPay = amountToPay - monthlyPayment + interest;
            }

            return loanRefund;
        }

        public List<AmortizationTableLine> GetAmortizationTable()
        {
            amortizationTable.table.Clear();

            amortizationTable.table.AddRange(GetDefferedAmotizationTable());
            amortizationTable.table.AddRange(GetAmotizationTableAfterDefered());

            return amortizationTable.table;
        }

        private List<AmortizationTableLine> GetDefferedAmotizationTable()
        {
            double amountToPay = loanAmount.Amount;
            double firstInterest = (amountToPay * loanInterest.InterestRate / 100 / months_per_year);
            uint defferedDurationInMonth = loanDefferedDuration.DefferedDuration;
            return FillAmortizationTable(1, defferedDurationInMonth, firstInterest, amountToPay);
        }
        private List<AmortizationTableLine> GetAmotizationTableAfterDefered()
        {
            double amountToPay = loanAmount.Amount;
            uint loanDurationInMonth = loanDuration.Duration * months_per_year;
            double monthlyPayment = GetMonthlyPaymentWithoutInsurance();
            uint endDefferedMonth = loanDefferedDuration.DefferedDuration + 1;
            return FillAmortizationTable(endDefferedMonth, loanDurationInMonth, monthlyPayment, amountToPay);
        }
        private List<AmortizationTableLine> FillAmortizationTable(uint startMonth, uint loanDurationInMonth, double monthlyPayment, double amountToPay)
        {
            List<AmortizationTableLine> table = new List<AmortizationTableLine>();
            double interest = 0.0;
            uint loanRefund = 0;

            for (uint monthNumber = startMonth; monthNumber <= loanDurationInMonth; monthNumber++)
            {
                interest = (amountToPay * loanInterest.InterestRate / 100 / months_per_year);
                loanRefund = (uint)(Math.Round(monthlyPayment) - Math.Round(interest));
                amountToPay = amountToPay - monthlyPayment + interest;

                AmortizationTableLine nextAmortizationLine = new AmortizationTableLine(monthNumber,
                    (uint)Math.Round(monthlyPayment),
                    (uint)Math.Round(interest),
                    loanRefund,
                    (uint)Math.Round(amountToPay));
                table.Add(nextAmortizationLine);
            }

            return table;
        }
    }
}
