﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace library_loan_simulator.Model.Loan
{
    public class LoanInsurance
    {
        public LoanInsurance()
        {
            insuranceRisks.Add(new InsuranceRisk("Sportif", -0.05));
            insuranceRisks.Add(new InsuranceRisk("Fumeur", 0.15));
            insuranceRisks.Add(new InsuranceRisk("Troubles cardiaques", 0.3));
            insuranceRisks.Add(new InsuranceRisk("Ingénieur Informatique", -0.05));
            insuranceRisks.Add(new InsuranceRisk("Pilote de chasse", 0.15));
        }

        public double InsuranceRate
        {
            get
            {
                return GetInsuranceRisksTotalRate();
            }
            set
            {
                if (value >= minimumInsuranceRate && value <= maximumInsuranceRate)
                {
                    insuranceRate = value;
                }
                else
                {
                    throw new ArgumentException($"Invalid insurance rate, value must be greater or equal to 0");
                }
            }
        }
        public List<InsuranceRisk> GetInsuranceRisks()
        {
            return insuranceRisks;
        }

        public double GetInsuranceRisksTotalRate()
        {
            return baseInsuranceRate + GetInsuranceRisksRate();
        }
        public void SetInsuranceRiskBoolFromRiskName(string riskName, bool value)
        {
            foreach (InsuranceRisk risk in insuranceRisks)
            {
                if (risk.RiskName == riskName)
                {
                    risk.IsRisk = value;
                }
            }
        }

        private double GetInsuranceRisksRate()
        {
            double rate = 0.0;
            foreach (InsuranceRisk risk in insuranceRisks)
            {
                if (risk.IsRisk)
                {
                    rate += risk.RiskRate;
                }
            }
            return rate;
        }

        private double insuranceRate;
        private List<InsuranceRisk> insuranceRisks = new List<InsuranceRisk>();

        
        private double baseInsuranceRate = 0.3;

        private double minimumInsuranceRate = 0.1;
        private double maximumInsuranceRate = 0.9;
    }
}
