﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace library_loan_simulator.Model.Loan
{
    public class LoanDefferedDuration
    {
        public LoanDefferedDuration()
        {
            LoanMaximumDefferedDuration = 24;
        }

        private uint defferedDuration = 0;
        public uint DefferedDuration
        {
            get
            {
                return defferedDuration;
            }
            set
            {
                if (value <= LoanMaximumDefferedDuration)
                {
                    defferedDuration = value;
                }
                else
                {
                    throw new ArgumentException($"Invalid loan duration, value must be lower or equal to {LoanMaximumDefferedDuration}");
                }
            }
        }
        private uint LoanMaximumDefferedDuration { get; set; }
    }
}
