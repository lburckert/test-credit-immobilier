﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace library_loan_simulator.Model.Loan
{
    public class LoanAmount
    {
        public LoanAmount()
        {
            LoanMininumAmount = 50000;
        }

        public uint Amount
        {
            get
            {
                return amount;
            }
            set
            {
                if (value >= LoanMininumAmount)
                {
                    amount = value;
                }
                else
                {
                    throw new ArgumentException($"Invalid loan amount, value must be greater or equal to {LoanMininumAmount}");
                }
            }
        }
        private uint amount;
        private uint LoanMininumAmount { get; set; }
    }
}
