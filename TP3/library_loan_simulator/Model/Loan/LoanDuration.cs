﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace library_loan_simulator.Model.Loan
{
    public class LoanDuration
    {
        public LoanDuration()
        {
            LoanMinimumDuration = 9;
            LoanMaximumDuration = 25;
        }

        private uint duration;
        public uint Duration
        {
            get
            {
                return duration;
            }
            set
            {
                if (value >= LoanMinimumDuration && value <= LoanMaximumDuration)
                {
                    duration = value;
                }
                else
                {
                    throw new ArgumentException($"Invalid loan duration, value must be greater or equal to {LoanMinimumDuration} and lower or equal to {LoanMaximumDuration}");
                }
            }
        }
        private uint LoanMinimumDuration { get; set; }
        private uint LoanMaximumDuration { get; set; }
    }
}
