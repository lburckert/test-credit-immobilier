﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace library_loan_simulator.Model.Loan
{
    public class InsuranceRisk
    {
        public InsuranceRisk(string riskName, double riskRate)
        {
            this.riskName = riskName;
            this.riskRate = riskRate;
            IsRisk = false;
        }
        public string RiskName { get { return riskName; } }
        public double RiskRate { get { return riskRate; } }
        public bool IsRisk { get; set; }

        private string riskName;
        private double riskRate;
    }
}
