﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using library_loan_simulator.Model.Loan;
using library_loan_simulator.Controller;

namespace unit_test_loan_simulator
{
    public class UnitTestLoan
    {
        [Fact]
        [Trait("Category","Exception")]
        private void SetLoanAmountException()
        {   
            LoanAmount loanAmount = new LoanAmount();

            Assert.Throws<ArgumentException>(() => loanAmount.Amount = 49999);
        }

        [Fact]
        [Trait("Category", "Exception")]
        private void SetLoanDurationException()
        {
            LoanDuration loanDuration = new LoanDuration();

            Assert.Throws<ArgumentException>(() => loanDuration.Duration = 8);
            Assert.Throws<ArgumentException>(() => loanDuration.Duration = 26);
        }

        [Fact]
        [Trait("Category", "Exception")]
        private void SetLoanInterestException()
        {
            LoanInterest loanInterest = new LoanInterest();

            Assert.Throws<ArgumentException>(() => loanInterest.InterestRate = -0.01);
        }

        [Fact]
        [Trait("Category", "Exception")]
        private void SetLoanInsuranceException()
        {
            LoanInsurance loanInsurance = new LoanInsurance();

            Assert.Throws<ArgumentException>(() => loanInsurance.InsuranceRate = 1);
            Assert.Throws<ArgumentException>(() => loanInsurance.InsuranceRate = 0);
        }

        [Fact]
        [Trait("Category", "Exception")]
        private void SetLoanDefferedDurationException()
        {
            LoanDefferedDuration loanDefferedDuration = new LoanDefferedDuration();

            Assert.Throws<ArgumentException>(() => loanDefferedDuration.DefferedDuration = 25);
        }

        [Theory]
        [InlineData("Sportif")]
        [InlineData("Fumeur")]
        [InlineData("Troubles cardiaques")]
        [Trait("Category", "Contains")]
        private void ContainsInsuranceRisks(string riskName)
        {
            LoanInsurance loanInsurance = new LoanInsurance();

            Assert.Contains(riskName, loanInsurance.GetInsuranceRisks().Select(risk => risk.RiskName));
        }

        [Fact]
        [Trait("Category", "Calculation")]
        private void GetInsuranceRisksTotalRate()
        {
            LoanInsurance loanInsurance = new LoanInsurance();

            loanInsurance.SetInsuranceRiskBoolFromRiskName("Fumeur", true);
            loanInsurance.SetInsuranceRiskBoolFromRiskName("Troubles cardiaques", true);
            loanInsurance.SetInsuranceRiskBoolFromRiskName("Ingénieur Informatique", true);

            Assert.Equal(0.7, loanInsurance.GetInsuranceRisksTotalRate());
        }

        [Fact]
        [Trait("Category", "Calculation")]
        private void CalculateMonthlyPaymentsCase1()
        {
            LoanController loanController = new LoanController();

            loanController.loanAmount.Amount = 175000;
            loanController.loanDuration.Duration = 25;
            loanController.loanInterest.InterestRate = 1.27;

            loanController.loanInsurance.SetInsuranceRiskBoolFromRiskName("Fumeur", true);
            loanController.loanInsurance.SetInsuranceRiskBoolFromRiskName("Troubles cardiaques", true);
            loanController.loanInsurance.SetInsuranceRiskBoolFromRiskName("Ingénieur Informatique", true);

            Assert.Equal(681, Math.Round(loanController.GetMonthlyPaymentWithoutInsurance()));
            Assert.Equal(102, Math.Round(loanController.GetMonthlyInsurancePayment()));
            Assert.Equal(783, Math.Round(loanController.GetMonthlyPaymentTotal()));
            Assert.Equal(29341, Math.Round(loanController.GetTotalInterestPayment()));
            Assert.Equal(30625, Math.Round(loanController.GetTotalInsurancePayment()));
            Assert.Equal(63402, Math.Round(loanController.GetRefundAmountAtYear(10)));

            List<AmortizationTableLine> amortizationTable = loanController.GetAmortizationTable();
            AmortizationTableLine firstLine = new AmortizationTableLine(1,681,185,496,174504);
            AmortizationTableLine lastLine = new AmortizationTableLine(300,681,1,680,0);
            Assert.True(IsAmortizationLineEqual(firstLine, amortizationTable[0]));
            Assert.True(IsAmortizationLineEqual(lastLine, amortizationTable[amortizationTable.Count - 1]));
        }        

        [Fact]
        [Trait("Category", "Calculation")]
        private void CalculateMonthlyPaymentsCase2()
        {
            LoanController loanController = new LoanController();

            loanController.loanAmount.Amount = 200000;
            loanController.loanDuration.Duration = 15;
            loanController.loanInterest.InterestRate = 0.73;

            loanController.loanInsurance.SetInsuranceRiskBoolFromRiskName("Sportif", true);
            loanController.loanInsurance.SetInsuranceRiskBoolFromRiskName("Pilote de chasse", true);

            Assert.Equal(1173, Math.Round(loanController.GetMonthlyPaymentWithoutInsurance()));
            Assert.Equal(67, Math.Round(loanController.GetMonthlyInsurancePayment()));
            Assert.Equal(1240, Math.Round(loanController.GetMonthlyPaymentTotal()));
            Assert.Equal(11211, Math.Round(loanController.GetTotalInterestPayment()));
            Assert.Equal(12000, Math.Round(loanController.GetTotalInsurancePayment()));
            Assert.Equal(130837, Math.Round(loanController.GetRefundAmountAtYear(10)));
            List<AmortizationTableLine> amortizationTable = loanController.GetAmortizationTable();
            AmortizationTableLine firstLine = new AmortizationTableLine(1, 1173, 122, 1051, 198948);
            AmortizationTableLine lastLine = new AmortizationTableLine(180, 1173, 1, 1172, 0);
            Assert.True(IsAmortizationLineEqual(firstLine, amortizationTable[0]));
            Assert.True(IsAmortizationLineEqual(lastLine, amortizationTable[amortizationTable.Count - 1]));
        }

        [Fact]
        [Trait("Category", "Calculation")]
        private void CalculateMonthlyPaymentsDeffered()
        {
            LoanController loanController = new LoanController();

            loanController.loanAmount.Amount = 175000;
            loanController.loanDuration.Duration = 25;
            loanController.loanInterest.InterestRate = 1.27;

            loanController.loanInsurance.SetInsuranceRiskBoolFromRiskName("Fumeur", true);
            loanController.loanInsurance.SetInsuranceRiskBoolFromRiskName("Troubles cardiaques", true);
            loanController.loanInsurance.SetInsuranceRiskBoolFromRiskName("Ingénieur Informatique", true);

            loanController.loanDefferedDuration.DefferedDuration = 12;

            Assert.Equal(705, Math.Round(loanController.GetMonthlyPaymentWithoutInsurance()));
            Assert.Equal(102, Math.Round(loanController.GetMonthlyInsurancePayment()));
            Assert.Equal(807, Math.Round(loanController.GetMonthlyPaymentTotal()));
            Assert.Equal(30337, Math.Round(loanController.GetTotalInterestPayment()));
            Assert.Equal(30625, Math.Round(loanController.GetTotalInsurancePayment()));
            Assert.Equal(59441, Math.Round(loanController.GetRefundAmountAtYear(10)));

            List<AmortizationTableLine> amortizationTable = loanController.GetAmortizationTable();
            AmortizationTableLine firstLine = new AmortizationTableLine(1, 185, 185, 0, 175000);
            AmortizationTableLine lastLine = new AmortizationTableLine(300, 705, 1, 704, 0);
            Assert.True(IsAmortizationLineEqual(firstLine, amortizationTable[0]));
            Assert.True(IsAmortizationLineEqual(lastLine, amortizationTable[amortizationTable.Count - 1]));
        }

        private bool IsAmortizationLineEqual(AmortizationTableLine firstLine, AmortizationTableLine amortizationTableLine)
        {
            if(firstLine.MonthNumber != amortizationTableLine.MonthNumber)
            {
                return false;
            }
            else if(firstLine.MonthlyPayment != amortizationTableLine.MonthlyPayment)
            {
                return false;
            }
            else if(firstLine.Interest != amortizationTableLine.Interest)
            {
                return false;
            }
            else if(firstLine.LoanRefund != amortizationTableLine.LoanRefund)
            {
                return false;
            }
            else if(firstLine.AmountToPay != amortizationTableLine.AmountToPay)
            {
                return false;
            }

            return true;
        }
    }
}
