﻿using library_loan_simulator.Controller;
using library_loan_simulator.Model.Loan;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace gui_loan_simulator.Pages
{
    /// <summary>
    /// Logique d'interaction pour LoanMenu.xaml
    /// </summary>
    public partial class LoanMenu : Page
    {
        Frame frame_main;
        LoanController loanController = new LoanController();

        public LoanMenu(Frame frame_main)
        {
            this.frame_main = frame_main;
            InitializeComponent();
            InitDataGridInsuranceRisks();
            InitDataGridAmortizationTable();
            lbl_InsuranceCalculatedRate.Content = "Taux final : " + loanController.loanInsurance.GetInsuranceRisksTotalRate();
        }

        private void Btn_returnToMenu_Click(object sender, RoutedEventArgs e)
        {
            frame_main.Content = new MainMenu(frame_main);
        }
        private void InitDataGridInsuranceRisks()
        {
            dataGrid_InsuranceRisks.ItemsSource = loanController.loanInsurance.GetInsuranceRisks();
            dataGrid_InsuranceRisks.AutoGenerateColumns = false;
            DataGridTextColumn textColumnRiskTitle = new DataGridTextColumn();
            textColumnRiskTitle.Header = "Intitulé";
            textColumnRiskTitle.Binding = new Binding("RiskName");
            textColumnRiskTitle.Width = 150;
            textColumnRiskTitle.IsReadOnly = true;

            DataGridTextColumn textColumnRiskRate = new DataGridTextColumn();
            textColumnRiskRate.Header = "Modification du taux";
            textColumnRiskRate.Binding = new Binding("RiskRate");
            textColumnRiskRate.Width = 75;
            textColumnRiskRate.IsReadOnly = true;

            DataGridCheckBoxColumn checkBoxColumnIsRisk = new DataGridCheckBoxColumn();
            checkBoxColumnIsRisk.Header = "oui/non";
            checkBoxColumnIsRisk.Binding = new Binding("IsRisk");
            checkBoxColumnIsRisk.Width = 75;

            dataGrid_InsuranceRisks.Columns.Add(textColumnRiskTitle);
            dataGrid_InsuranceRisks.Columns.Add(textColumnRiskRate);
            dataGrid_InsuranceRisks.Columns.Add(checkBoxColumnIsRisk);
        }

        private void InitDataGridAmortizationTable()
        {
            data_grid_amortization_table.AutoGenerateColumns = false;
            data_grid_amortization_table.IsReadOnly = true;
            DataGridTextColumn textColumnMonthNumber = new DataGridTextColumn();
            textColumnMonthNumber.Header = "Mois";
            textColumnMonthNumber.Binding = new Binding("MonthNumber");
            textColumnMonthNumber.Width = 50;

            DataGridTextColumn textColumnMonthlyPayment = new DataGridTextColumn();
            textColumnMonthlyPayment.Header = "Mensualité (hors assurance)";
            textColumnMonthlyPayment.Binding = new Binding("MonthlyPayment");
            textColumnMonthlyPayment.Width = 175;

            DataGridTextColumn textColumnInterest = new DataGridTextColumn();
            textColumnInterest.Header = "Intérêts";
            textColumnInterest.Binding = new Binding("Interest");
            textColumnInterest.Width = 75;

            DataGridTextColumn textColumnLoanRefund = new DataGridTextColumn();
            textColumnLoanRefund.Header = "Capital amorti";
            textColumnLoanRefund.Binding = new Binding("LoanRefund");
            textColumnLoanRefund.Width = 125;

            DataGridTextColumn textColumnAmountToPay = new DataGridTextColumn();
            textColumnAmountToPay.Header = "Reste dû";
            textColumnAmountToPay.Binding = new Binding("AmountToPay");
            textColumnAmountToPay.Width = 150;

            data_grid_amortization_table.Columns.Add(textColumnMonthNumber);
            data_grid_amortization_table.Columns.Add(textColumnMonthlyPayment);
            data_grid_amortization_table.Columns.Add(textColumnInterest);
            data_grid_amortization_table.Columns.Add(textColumnLoanRefund);
            data_grid_amortization_table.Columns.Add(textColumnAmountToPay);
        }
        private void DataGrid_InsuranceRisks_CurrentCellChanged(object sender, EventArgs e)
        {
            lbl_InsuranceCalculatedRate.Content = "Taux final : " + Math.Round(loanController.loanInsurance.GetInsuranceRisksTotalRate(), 2).ToString();
        }
        private void Btn_calculateResults_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                loanController.loanAmount.Amount = uint.Parse(txt_LoanAmount.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Le montant du prêt est invalide. La valeur doit être supérieure à 50000 €.");
                return;
            }
            try
            {
                loanController.loanDuration.Duration = uint.Parse(txt_LoanDuration.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("La durée du prêt est invalide. La valeur doit être comprise entre 9 et 25 ans (bornes incluses).");
                return;
            }
            try
            {
                loanController.loanInterest.InterestRate = double.Parse(txt_LoanRate.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Le taux d'intérêt du prêt est invalide. La valeur doit être supérieure ou égale à 0.");
                return;
            }
            try
            {
                loanController.loanDefferedDuration.DefferedDuration = uint.Parse(txt_LoanDefferedDuration.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("La durée du différé est invalide. La valeur doit être comprise entre 0 et 24 mois (bornes incluses).");
                return;
            }
            data_grid_amortization_table.ItemsSource = new ObservableCollection<AmortizationTableLine>(loanController.GetAmortizationTable());

            lbl_MonthlyTotalPayment.Content = "Mensualités totales : " + Math.Round(loanController.GetMonthlyPaymentTotal()).ToString() + " €";
            lbl_InsuranceMonthlyPayment.Content = "Mensualité assurance : " + Math.Round(loanController.GetMonthlyInsurancePayment()).ToString() + " €";
            lbl_MonthlyLoanPayment.Content = "Mensualité hors assurance : " + Math.Round(loanController.GetMonthlyPaymentWithoutInsurance()).ToString() + " €";
            lbl_TotalInterest.Content = "Montant total des intérêts : " + Math.Round(loanController.GetTotalInterestPayment()).ToString() + " €";
            lbl_TotalInsurance.Content = "Montant total de l'assurance : " + Math.Round(loanController.GetTotalInsurancePayment()).ToString() + " €";
            lbl_PaymentTenYears.Content = "Montant remboursé au bout de 10 ans : " + Math.Round((loanController.GetRefundAmountAtYear(10))).ToString() + " €";
        }

        private void Txt_LoanAmount_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            TextBox textBox = (TextBox)sender;
            e.Handled = Regex.IsMatch(e.Text, "[^0-9]+");
        }

        private void Txt_LoanDuration_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            TextBox textBox = (TextBox)sender;
            e.Handled = Regex.IsMatch(e.Text, "[^0-9]+");
        }

        private void Txt_LoanRate_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            TextBox textBox = (TextBox)sender;
            e.Handled = Regex.IsMatch(e.Text, "[^0-9,]+");
        }
    }
}
