﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace gui_loan_simulator.Pages
{
    /// <summary>
    /// Logique d'interaction pour MainMenu.xaml
    /// </summary>
    public partial class MainMenu : Page
    {
        Frame frame_main;
        public MainMenu(Frame frame_main)
        {
            this.frame_main = frame_main;
            InitializeComponent();
        }

        private void btn_loanMenu_Click(object sender, RoutedEventArgs e)
        {
            frame_main.Content = new LoanMenu(frame_main);
        }

        private void btn_quit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}
